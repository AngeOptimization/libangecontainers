#ifndef EQUIPMENT_NUMBER_H
#define EQUIPMENT_NUMBER_H

#include "angecontainers_export.h"

#include <QList>
#include <QMetaType>
#include <QString>

namespace ange {
namespace containers {

/**
 * Class for storing an equipment number
 * handles standardization of imported equipment numbers
 */
class ANGECONTAINERS_EXPORT EquipmentNumber : public QString {

public:
    EquipmentNumber(const QString& equipment_number);
    EquipmentNumber() : QString() {};

    /**
     * In case equipment number is not known - load list projections for example - generate random EQD ID
     * @param length number of ID's in returned list
     * @return list of equipment numbers with random start number and following incremental numbers
     * */
    static QList<EquipmentNumber>  generateRandomEquipmentNumbers(int length);
};

}} // namespace

Q_DECLARE_METATYPE(ange::containers::EquipmentNumber)


#ifdef QTEST_VERSION
namespace QTest {

template<>
inline char* toString(const ange::containers::EquipmentNumber& equipmentNumber) {
    return QTest::toString<QString>(equipmentNumber);
}

}  // namespace QTest
#endif  // QTEST_VERSION


#endif // EQUIPMENT_NUMBER_H
