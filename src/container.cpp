#include "container.h"
#include "dangerousgoodscode.h"
#include "oog.h"
#include "equipmentnumber.h"

using namespace ange::units;
using ange::utils::Identifiable;

namespace ange {
namespace containers {

class ContainerPrivate {
public:
    ContainerPrivate(const EquipmentNumber& equipment_number,
                     Mass weight,
                     bool weightIsVerified,
                     const IsoCode& isocode,
                     Container::LiveStatus live,
                     double reefer_temperature,
                     bool empty,
                     bool dangerous,
                     const QList<DangerousGoodsCode>& dangerous_goods_codes,
                     const QString booking_number,
                     QString carrier_code
                    ) :
        m_equipment_number(equipment_number),
        m_weight(weight),
        m_weightIsVerified(weightIsVerified),
        m_isocode(isocode),
        m_live(live),
        m_reefer_temperature(reefer_temperature),
        m_empty(empty),
        m_dangerous(dangerous),
        m_dangerous_goods_codes(dangerous_goods_codes),
        m_booking_number(booking_number),
        m_height_in_meters(-1),
        m_carrier_code(carrier_code),
        m_containerBundleParent(0)
    {
        Q_ASSERT(m_dangerous != m_dangerous_goods_codes.isEmpty());
    }
    EquipmentNumber m_equipment_number;
    Mass m_weight;
    bool m_weightIsVerified;
    IsoCode m_isocode;
    Container::LiveStatus m_live;
    double m_reefer_temperature; // Celsius
    bool m_empty;
    bool m_dangerous;
    QList<DangerousGoodsCode> m_dangerous_goods_codes;
    QString m_load_port;
    QString m_discharge_port;
    Oog m_oog;
    QList<HandlingCode> m_handling_codes;
    QString m_booking_number;
    Length m_height_in_meters;
    QString m_carrier_code;
    Container* m_containerBundleParent;
    QList<Container*> m_bundleChildren;
    QString m_placeOfDelivery;
};

class ContainerUndoBlob {
public:
    ContainerUndoBlob(ContainerPrivate* other) : m_container_private(new ContainerPrivate(*other)) {
        // Empty
    }
    virtual ~ContainerUndoBlob() {
        delete m_container_private;
    }
    ContainerPrivate* m_container_private;
};

Container::Container(const containers::EquipmentNumber& equipmentNumber, Mass weight, const containers::IsoCode& isoCode)
  : QObject(), Identifiable<Container>(-1),
    d(new ContainerPrivate(equipmentNumber, weight, false, isoCode, NonLive, unknownTemperature(), false,
                           false, QList<DangerousGoodsCode>(), QString(), QString()))
{
    // Empty
}

Container::Container(ContainerUndoBlob* undo_data, int id)
  : QObject(), Identifiable<Container>(id), d(new ContainerPrivate(*(undo_data->m_container_private)))
{
    // Empty
}

Container::Container(ContainerPrivate* dpoint, int id)
  : QObject(), Identifiable<Container>(id), d(dpoint)
{
    // Empty
}

Container::~Container() {
    // Empty
}

Container* Container::createFromUndoData(ContainerUndoBlob* undo_data, int id) {
    return new Container(undo_data, id);
}

ContainerUndoBlob* Container::undoData() const {
    if (!d->m_bundleChildren.isEmpty()) {
        qWarning("You shouldn't create undo commands for containers with bundled children");
        Q_ASSERT(false);
    }
    if (d->m_containerBundleParent) {
        qWarning("you shouldn't create undo commandsn for containers that are part of a bundle. you must unbundle first");
        Q_ASSERT(false);
    }
    ContainerUndoBlob* undoblob = new ContainerUndoBlob(d.data());
    // better sanitize than crash later. For devs, the above assert should have been hit already.
    undoblob->m_container_private->m_bundleChildren.clear();
    undoblob->m_container_private->m_containerBundleParent = 0;
    return undoblob;
}

void Container::deleteUndoData(ContainerUndoBlob* undo_data) {
    delete undo_data;
}

QDebug operator<<(QDebug dbg, const Container& container) {
    return dbg << container.equipmentNumber() << container.isoCode().code() << container.weight() / kilogram << "kg";
}

QDebug operator<<(QDebug dbg, const Container* container) {
    if (container) {
        return dbg << *container;
    } else {
        return dbg << "<null>";
    }
}

units::Length Container::physicalHeight() const {
    if (d->m_height_in_meters / meter == -1) {
        return d->m_isocode.physicalHeight() + d->m_oog.top();
    } else {
        return d->m_height_in_meters;
    }
}

bool Container::isCollapsible() const {
    if (!empty())
        return false;
    return d->m_isocode.isCollapsible();
}

bool Container::isBundlable() const {
    if (!empty())
        return false;
    return d->m_isocode.isBundlable();
}

units::Length Container::physicalLength() const {
    return d->m_isocode.physicalLength();
}

double Container::temperature() const {
    return d->m_reefer_temperature;
}

void Container::setTemperature(double temperature) {
    if (!qFuzzyCompare(d->m_reefer_temperature, temperature)) {
        d->m_reefer_temperature = temperature;
        featureChanged(this, Temperature);
    }
}

IsoCode::IsoLength Container::isoLength() const {
    return d->m_isocode.length();
}

const containers::IsoCode& Container::isoCode() const {
    return d->m_isocode;
}

Mass Container::weight() const {
    return d->m_weight;
}

bool Container::weightIsVerified() const {
    return d->m_weightIsVerified;
}

EquipmentNumber Container::equipmentNumber() const {
    return d->m_equipment_number;
}

bool Container::empty() const {
    return d->m_empty;
}

bool Container::isDangerous() const {
    return !d->m_dangerous_goods_codes.isEmpty();
}

Container::LiveStatus Container::live() const {
    return d->m_live;
}

const QList<DangerousGoodsCode>& Container::dangerousGoodsCodes() const {
    return d->m_dangerous_goods_codes;
}

QString Container::dischargePort() const {
    return d->m_discharge_port;
}

QString Container::loadPort() const {
    return d->m_load_port;
}

void Container::setDischargePort(const QString& uncode) {
    if (d->m_discharge_port != uncode) {
        d->m_discharge_port = uncode;
        emit featureChanged(this, DischargePort);
    }
}

void Container::setEquipmentNumber(const containers::EquipmentNumber& equipmentNumber) {
    if (d->m_equipment_number != equipmentNumber) {
        d->m_equipment_number = equipmentNumber;
        emit featureChanged(this, EquipmentNumber);
    }
}

void Container::setBookingNumber(const QString& bookingNumber) {
    if (d->m_booking_number != bookingNumber) {
        d->m_booking_number = bookingNumber;
        emit featureChanged(this, BookingNumber);
    }
}

void Container::setLoadPort(const QString& unCode) {
    if (d->m_load_port != unCode) {
        d->m_load_port = unCode;
        emit featureChanged(this, LoadPort);
    }
}

void Container::setWeight(Mass weight) {
    if (d->m_weight != weight) {
        d->m_weight = weight;
        emit featureChanged(this, Weight);
    }
}

void Container::setWeightIsVerified(bool VGMStatus) {
    if (d->m_weightIsVerified != VGMStatus) {
        d->m_weightIsVerified = VGMStatus;
        emit featureChanged(this, VGM);
    }
}

void Container::setLive(Container::LiveStatus live) {
    if (d->m_live != live) {
        d->m_live = live;
        emit featureChanged(this, LiveFeature);
    }
}

void Container::setEmpty(bool empty) {
    if (d->m_empty != empty) {
        d->m_empty = empty;
        emit featureChanged(this, Empty);
    }
}

void Container::setCarrierCode(const QString& carrierCode) {
    if (d->m_carrier_code != carrierCode) {
        d->m_carrier_code = carrierCode;
        emit featureChanged(this, Carrier);
    }
}

void Container::setIsoHeight(IsoCode::IsoHeight height) {
    if (d->m_isocode.height() != height) {
        d->m_isocode.setHeight(height);
        emit featureChanged(this, Height);
        emit featureChanged(this, IsoCode);
    }
}

void Container::setIsoLength(IsoCode::IsoLength length) {
    if (d->m_isocode.length() != length) {
        d->m_isocode.setLength(length);
        emit featureChanged(this, Length);
        emit featureChanged(this, IsoCode);
    }
}

void Container::setIsoCode(const containers::IsoCode& isoCode) {
    if (d->m_isocode != isoCode) {
        d->m_isocode = isoCode;
        emit featureChanged(this, IsoCode);
        emit featureChanged(this, Reefer);
        emit featureChanged(this, Height);
        emit featureChanged(this, Length);
    }
}

containers::Oog Container::oog() const {
    return d->m_oog;
}

void Container::setOog(const containers::Oog& oog) {
    d->m_oog = oog;
    emit featureChanged(this, Oog);
}

void Container::setPhysicalHeight(units::Length height) {
    if (!qFuzzyCompare(height / meter, d->m_height_in_meters / meter)) {
        d->m_height_in_meters = height;
        emit featureChanged(this, Height);
    }
}

bool Container::open() const {
    return d->m_isocode.isOpen();
}

void Container::addDangerousGoodsCode(const DangerousGoodsCode& dangerousGoodsCode) {
    d->m_dangerous_goods_codes << dangerousGoodsCode;
    d->m_dangerous = true;
    emit featureChanged(this, DangerousGoodsCodes);
}

void Container::setDangerousGoodsCodes(const QList<DangerousGoodsCode>& dangerousGoodsCodes) {
    if (d->m_dangerous_goods_codes != dangerousGoodsCodes) {
        d->m_dangerous_goods_codes = dangerousGoodsCodes;
        d->m_dangerous = !dangerousGoodsCodes.empty();
        emit featureChanged(this, DangerousGoodsCodes);
    }
}

QString Container::bookingNumber() const {
    return d->m_booking_number;
}

QString Container::carrierCode() const {
    return d->m_carrier_code;
}

void Container::setHandlingCodes(QList<HandlingCode> handlingCodes) {
    if (d->m_handling_codes != handlingCodes) {
        d->m_handling_codes = handlingCodes;
        emit featureChanged(this, HandlingCodes);
    }
}

void Container::addHandlingCode(HandlingCode handlingCode) {
    d->m_handling_codes << handlingCode;
    emit featureChanged(this, HandlingCodes);
}

QList<HandlingCode> Container::handlingCodes() const {
    return d->m_handling_codes;
}

QList<Container*> Container::bundleChildren() const {
    return d->m_bundleChildren;
}

bool Container::bundleContainer(Container* container) {
    if (container->d->m_containerBundleParent) {
        return false;
    } else {
        container->d->m_containerBundleParent = this;
        d->m_bundleChildren << container;
        emit bundleStatusChanged(this);
        emit bundleStatusChanged(container);
        return true;
    }
}

Container* Container::bundleParent() const {
    return d->m_containerBundleParent;
}

bool Container::unbundleContainer(Container* container) {
    if (container->d->m_containerBundleParent) {
        // has a parent
        if (container->d->m_containerBundleParent == this) {
            container->d->m_containerBundleParent = 0;
            d->m_bundleChildren.removeAll(container);
            emit bundleStatusChanged(this);
            emit bundleStatusChanged(container);
            return true;
        } else {
            // wopsie trying to unbundle someone elses container
            return false;
        }
    } else {
        return false;
    }
}

// static
Length Container::standardContainerWidth() {
    return 8 * feet;
}

double Container::equivalentTEU(double HCequivalenceFactor) const {
    return (isoLength() == IsoCode::Twenty ? 1.0 : 2.0)
           * (d->m_isocode.height() == IsoCode::HC ? HCequivalenceFactor / 2 : 1.0);
}

QString Container::placeOfDelivery() const {
    return d->m_placeOfDelivery;
}

void Container::setPlaceOfDelivery(const QString& uncode) {
    if (d->m_placeOfDelivery != uncode) {
        d->m_placeOfDelivery = uncode;
        emit featureChanged(this, PlaceOfDelivery);
    }
}

}} // namespace ange::container
