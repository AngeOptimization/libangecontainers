#include "oog.h"

#include <QSharedData>

using namespace ange::units;

namespace ange {
namespace containers {

class Oog::OogData : public QSharedData {
public:
    OogData()
      : front(0.0), back(0.0), right(0.0), left(0.0), top(0.0)
    {
        // Empty
    }
    OogData(const OogData& other)
      : QSharedData(other), front(other.front), back(other.back), right(other.right), left(other.left), top(other.top)
    {
        // Empty
    }
    bool hasNonZero() const {
        return front != 0*meter || back != 0*meter || right != 0*meter || left != 0*meter || top != 0*meter;
    }
    Length front;
    Length back;
    Length right;
    Length left;
    Length top;
};

Oog::Oog() : d(new OogData()) {
    // Empty
}

Oog::Oog(const Oog& other) : d(other.d) {
    // Empty
}

Oog::~Oog() {
    // Empty
}

Oog& Oog::operator=(const Oog& rhs) {
    if (this == &rhs) {  // Protect against self-assignment
        return *this;
    }
    d = rhs.d;
    return *this;
}

Oog::operator bool() const {
    return d->hasNonZero();
}

Length Oog::front() const {
    return d->front;
}

Length Oog::back() const {
    return d->back;
}

Length Oog::right() const {
    return d->right;
}

Length Oog::left() const {
    return d->left;
}

Length Oog::top() const {
    return d->top;
}

void Oog::setFront(Length front) {
    d->front = front;
}

void Oog::setBack(Length back) {
    d->back = back;
}

void Oog::setRight(Length right) {
    d->right = right;
}

void Oog::setLeft(Length left) {
    d->left = left;
}

void Oog::setTop(Length top) {
    d->top = top;
}

} //namespace containers
} //namespace ange
