#include "imoclassesperunnumber.h"

#include <QList>
#include <QFile>
#include <QRegExp>
#include <QTextStream>

QHash<QString, QStringList >* ImoClassesPerUnNumber::s_imoClassesPerUnNumber = 0;

const QHash<QString, QStringList>* ImoClassesPerUnNumber::instance() {
    if (!s_imoClassesPerUnNumber) {
        s_imoClassesPerUnNumber = new QHash<QString, QStringList>();
        initializeClassesPerUnNumber();
    }
    return s_imoClassesPerUnNumber;
}

void ImoClassesPerUnNumber::initializeClassesPerUnNumber() {
    QFile un_numberDataFile(":/data/classes_per_un_number.dat");
    un_numberDataFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream textStream(&un_numberDataFile);
    while (!textStream.atEnd()) {
        QString next_line = textStream.readLine();
        QRegExp unNumberRx("^(\\d{4})\\t([^\\s]*)$");
        unNumberRx.indexIn(next_line);
        const QString un_number = unNumberRx.cap(1);
        const QString classOrSubstance = unNumberRx.cap(2);
        s_imoClassesPerUnNumber->operator[](un_number) << classOrSubstance;
    }
    un_numberDataFile.close();
}

ImoClassesPerUnNumber::~ImoClassesPerUnNumber() {
    // FIXME this is never called!
    Q_ASSERT(false);
}
