#ifndef IMO_CLASSES_PER_UNNUMBER_H
#define IMO_CLASSES_PER_UNNUMBER_H

#include "angecontainers_export.h"

#include <QHash>
#include <QString>
#include <QStringList>

/**
 * A singleton providing the IMO classes contained in each UN number
 */
class ImoClassesPerUnNumber {

public:

    /**
     * Returns pointer to the singleton QHash, only instantiates on first call.
     */
    static const QHash<QString, QStringList>* instance();

private:
    // Hide constructor
    ImoClassesPerUnNumber();

    // FIXME This is never called
    ~ImoClassesPerUnNumber();

    static QHash<QString, QStringList>* s_imoClassesPerUnNumber;
    static void initializeClassesPerUnNumber();

};

#endif // IMO_CLASSES_PER_UNNUMBER_H
