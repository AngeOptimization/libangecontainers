add_library(Containers SHARED
    resources/data.qrc
    container.cpp
    dangerousgoodscode.cpp
    equipmentnumber.cpp
    handlingcode.cpp
    imoclassesperunnumber.cpp
    isocode.cpp
    oog.cpp
)
target_link_libraries(Containers
    Ange::Utils
    Qt5::Core
)

generate_export_header(Containers BASE_NAME AngeContainers)

set_property(TARGET Containers PROPERTY VERSION ${SO_VERSION}.0.0)
set_property(TARGET Containers PROPERTY SOVERSION ${SO_VERSION})
set_property(TARGET Containers PROPERTY OUTPUT_NAME AngeContainers)

install(TARGETS Containers EXPORT AngeContainersTargets
    RUNTIME DESTINATION "bin"
    LIBRARY DESTINATION "lib"
    ARCHIVE DESTINATION "lib"
    INCLUDES DESTINATION "include/AngeContainers"
)

install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/angecontainers_export.h
    container.h
    dangerousgoodscode.h
    equipmentnumber.h
    handlingcode.h
    imoclassesperunnumber.h
    isocode.h
    oog.h
    DESTINATION "include/AngeContainers/ange/containers"
)

write_basic_package_version_file(
    ${CMAKE_CURRENT_BINARY_DIR}/AngeContainersConfigVersion.cmake
    COMPATIBILITY SameMajorVersion
)

configure_file(
    AngeContainersConfig.cmake.in
    AngeContainersConfig.cmake
    @ONLY
)

set(ConfigPackageLocation lib/cmake/AngeContainers)
install(EXPORT AngeContainersTargets NAMESPACE Ange::
    FILE AngeContainersTargets.cmake
    DESTINATION ${ConfigPackageLocation}
)
install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/AngeContainersConfigVersion.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/AngeContainersConfig.cmake
    DESTINATION ${ConfigPackageLocation}
)

add_subdirectory(test)
