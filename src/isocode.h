#ifndef ANGE_CONTAINERS_ISOCODE_H
#define ANGE_CONTAINERS_ISOCODE_H

#include "angecontainers_export.h"

#include <ange/units/units.h>

#include <QList>
#include <QSharedDataPointer>
#include <QString>

namespace ange {
namespace containers {

/**
 * Class for storing iso codes.
 */
class ANGECONTAINERS_EXPORT IsoCode {

public:

    enum Standard {
        InvalidCode = 0,  // Can be used to check for validity, this is false
        Standard1984 = 84,  // Code conforms to ISO 6346:1984
        Standard1995 = 95,  // Code conforms to ISO 6346:1995
    };

    enum IsoLength {
        UnknownLength = -1,
        Twenty = 20,
        Fourty = 40,
        FourtyFive = 45,
        FiftyThree = 53,
    };

    enum IsoHeight {
        UnknownHeight = -1,
        DC = 1,  // 8.5'
        HC = 2,  // 9.5'
        HalfLC = 3,  // 4'
        HalfDC = 4,  // 4.5'
        LC = 5,  // 8'
        MC = 6,  // 9'
    };

    /**
     * Construct isocode from code string (e.g. 22GT)
     */
    explicit IsoCode(const QString& code);

    /**
     * Construct IsoCode from data. Note that such is not unique
     */
    IsoCode(IsoLength length, IsoHeight height, bool reefer);

    IsoCode(const IsoCode &other);

    ~IsoCode();

    IsoCode &operator=(const IsoCode &rhs);

    bool operator==(const IsoCode& rhs) const;

    bool operator!=(const IsoCode& rhs) const {
        return !operator==(rhs);
    }

    /**
     * @return code (e.g. 22GT) as a string
     */
    QString code() const;

    /**
     * Change the code
     */
    void setCode(const QString& code);

    /**
     * @return which standard this code conforms to
     */
    Standard standard() const;

    /**
     * @return length (as enum) of container
     */
    IsoLength length() const ;

    /**
     * Sets the length of container
     */
    void setLength(IsoLength length);

    /**
     * @return length (physical) of length enum. Defaults to 40' if length cannot be determined
     */
    static ange::units::Length physicalLength(IsoLength length);

    /**
     * @return length (physical) of container. Defaults to 40' if length cannot be determined
     */
    ange::units::Length physicalLength() const;

    /**
     * @return height of container
     */
    IsoHeight height() const ;

    /**
     * Sets the raw height of container (excluding OOG height)
     */
    void setHeight(IsoHeight height);

    /**
     * @return height of container in meters based on iso code only,
     * i.e. not taking Out Of Gauge (OOG) dimensions into account
     */
    static ange::units::Length physicalHeight(IsoHeight height);

    /**
     * @return height of container in meters based on iso code only,
     */
    ange::units::Length physicalHeight() const;

    /**
     * @return whether this isocode is a reefer container.
     */
    bool reefer() const;

    /**
     * Sets whether the container is a reefer or not
     */
    void setReefer(bool reefer);

    /**
     * @return true if iso code is a collapsible flatrack or a plain platform
     */
    bool isBundlable() const;

    /**
     * @return true if iso code is a collapsible flatrack
     */
    bool isCollapsible() const;

    /**
     * @returns true if open top, platform/flatracks and ventilated, they are considered open for DG rules
     **/
    bool isOpen() const;

    /**
     * Return a list of known lengths that is widely used, this list is used by the container modification dialog
     */
    static QList<IsoLength> knownLengths();

    /**
     * Return a list of all possible values of IsoLength, this includes the unknown length
     */
    static QList<IsoLength> allLengths();

    /**
     * Transform a length to the string representing it
     */
    static QString lengthToString(IsoLength length);

    /**
     * Return a list of heights that is widely used, this list is used by the container modification dialog
     */
    static QList<IsoHeight> knownHeights();

    /**
     * Transform a length to the string representing it
     */
    static QString heightToString(IsoHeight height);

private:
    class IsoCodePrivate;
    QSharedDataPointer<IsoCodePrivate> d;

};

}} // namespace ange::containers

uint qHash(const ange::containers::IsoCode& isoCode);

Q_DECLARE_METATYPE(ange::containers::IsoCode::Standard)
Q_DECLARE_METATYPE(ange::containers::IsoCode::IsoLength)
Q_DECLARE_METATYPE(ange::containers::IsoCode::IsoHeight)


#ifdef QTEST_VERSION
namespace QTest {

template<>
inline char* toString(const ange::containers::IsoCode::IsoLength& length) {
    return QTest::toString<QString>(ange::containers::IsoCode::lengthToString(length));
}

template<>
inline char* toString(const ange::containers::IsoCode::IsoHeight& height) {
    return QTest::toString<QString>(ange::containers::IsoCode::heightToString(height));
}

}  // namespace QTest
#endif  // QTEST_VERSION


#endif  // ANGE_CONTAINERS_ISOCODE_H
