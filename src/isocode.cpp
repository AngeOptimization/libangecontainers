#include "isocode.h"

#include <QHash>

using namespace ange::units;

namespace ange {
namespace containers {

class IsoCode::IsoCodePrivate : public QSharedData {
public:
    void makeCodeStandard();
public:
    QString code;
};

void IsoCode::IsoCodePrivate::makeCodeStandard() {
    code = code.trimmed();
    if (code.size() <= 4) {  // code is short or correct size, append X'es
        while (code.size() < 4) {
            code += 'X';
        }
    } else {  // code is long, trim it
        code = code.left(4);
    }
}

IsoCode::IsoCode(const QString& code)
  : d(new IsoCodePrivate)
{
    d->code = code;
}

IsoCode::IsoCode(IsoCode::IsoLength length, IsoCode::IsoHeight height, bool reefer)
  : d(new IsoCodePrivate)
{
    d->code = QStringLiteral("XXGP");  // ensure ISO 6346:1995 starting point
    setLength(length);
    setHeight(height);
    setReefer(reefer);
}

IsoCode::IsoCode(const IsoCode& other)
  : d(other.d)
{
    // Empty
}

IsoCode::~IsoCode() {
    // Empty
}

IsoCode& IsoCode::operator=(const IsoCode& rhs) {
    if (this == &rhs) {  // Protect against self-assignment
        return *this;
    }
    d = rhs.d;
    return *this;
}

bool IsoCode::operator==(const IsoCode& rhs) const {
    return d->code == rhs.d->code;
}

QString IsoCode::code() const {
    return d->code;
}

void IsoCode::setCode(const QString& code) {
    d->code = code;
}

IsoCode::Standard IsoCode::standard() const {
    if (d->code.size() != 4) {
        return InvalidCode;
    }
    if (d->code.at(2).isDigit()) {
        return Standard1984;
    } else {
        return Standard1995;
    }
}

IsoCode::IsoLength IsoCode::length() const {
    switch (standard()) {
        case InvalidCode:
            return UnknownLength;
        case Standard1984: {
            switch (d->code.at(0).unicode()) {
                case '2':
                    return Twenty;
                case '4':
                    return Fourty;
                case '9':  // l > 40' (notice error in some documents, where this is described as l < 40' !)
                    return FourtyFive;
                    // no support for FIFTYTHREE in 1984
            }
            return UnknownLength;
        }
        case Standard1995: {
            switch (d->code.at(0).unicode()) {
                case '2':
                    return Twenty;
                case '4':
                    return Fourty;
                case 'L':
                    return FourtyFive;
                case 'P':
                    return FiftyThree;
            }
            return UnknownLength;
        }
    }
    Q_ASSERT(false);
    return UnknownLength;
}

void IsoCode::setLength(IsoLength length) {
    if (length == this->length()) {
        return;
    }
    Standard standard = this->standard();
    if (standard == InvalidCode) {
        d->makeCodeStandard();
        standard = this->standard();
    }
    switch (standard) {
        case InvalidCode:
            Q_ASSERT(false);
            return;
        case Standard1984: {
            switch (length) {
                case Twenty:
                    d->code[0] = '2';
                    return;
                case Fourty:
                    d->code[0] = '4';
                    return;
                case FourtyFive:
                    d->code[0] = '9';
                    return;
                case FiftyThree:
                case UnknownLength:
                    d->code[0] = '0';
                    return;
            }
            Q_ASSERT(false);
            return;
        }
        case Standard1995: {
            switch (length) {
                case Twenty:
                    d->code[0] = '2';
                    return;
                case Fourty:
                    d->code[0] = '4';
                    return;
                case FourtyFive:
                    d->code[0] = 'L';
                    return;
                case FiftyThree:
                    d->code[0] = 'P';
                    return;
                case UnknownLength:
                    d->code[0] = 'X';
                    return;
            }
            Q_ASSERT(false);
            return;
        }
    }
    Q_ASSERT(false);
    return;
}

// static
Length IsoCode::physicalLength(IsoCode::IsoLength length) {
    switch (length) {
        case Twenty:
            return 20 * feet;
        case Fourty:
            return 40 * feet;
        case FourtyFive:
            return 45 * feet;
        case FiftyThree:
            return 53 * feet;
        case UnknownLength:
            return 40 * feet;
    }
    Q_ASSERT(false);
    return 40 * feet;
}

Length IsoCode::physicalLength() const {
    return physicalLength(length());
}

IsoCode::IsoHeight IsoCode::height() const {
    switch (standard()) {
        case InvalidCode:
            return UnknownHeight;
        case Standard1984: {
            switch (d->code.at(1).unicode()) {
                case '0':
                case '1':
                    return LC; // 8'0"
                case '2':
                case '3':
                    return DC; // 8'6"
                // MC 9'0" not part of 1984
                case '4': // 4 & 5 are h > 8'6"
                case '5':
                    return HC; // 9'6"
                case '6': // 6 & 7 are 4'0" < h < 4'3"
                case '7':
                    return HalfDC; // 4'3"
                case '8': // 8 is 4'3" < h < 8'0"
                    return LC; // 8'0"
                case '9':
                    return HalfLC; // < 4'0"
            }
            return UnknownHeight;
        }
        case Standard1995: {
            switch (d->code.at(1).unicode()) {
                case '0':
                    return LC; // 8'0"
                case '2':
                case 'C':
                case 'L':
                    return DC; // 8'6"
                case '4':
                case 'D':
                case 'M':
                    return MC; // 9'0"
                case '5':
                case 'E':
                case 'N':
                    return HC; // 9'6"
                case '8':
                    return HalfDC; // 4'3"
                case '9':
                    return HalfLC; // <= 4'0"
            }
            return UnknownHeight;
        }
    }
    Q_ASSERT(false);
    return UnknownHeight;
}

void IsoCode::setHeight(IsoHeight height) {
    if (height == this->height()) {
        return;
    }
    Standard standard = this->standard();
    if (standard == InvalidCode) {
        d->makeCodeStandard();
        standard = this->standard();
    }
    switch (standard) {
        case InvalidCode:
            Q_ASSERT(false);
            return;
        case Standard1984: {
            switch (height) {
                case LC:
                    d->code[1] = '0'; // 8'0"
                    return;
                case DC:
                    d->code[1] = '2'; // 8'6"
                    return;
                case HC:
                case MC: // MC 9'0" not part of 1984
                    d->code[1] = '4'; // h > 8'6”
                    return;
                case HalfDC:
                    d->code[1] = '6';  // 4'0” < h < 4'3”
                    return;
                case HalfLC:
                    d->code[1] = '9'; // h < 4'0"
                    return;
                case UnknownHeight:
                    d->code[1] = 'X';
                    return;
            }
            Q_ASSERT(false);
            return;
        }
        case Standard1995: {
            switch (height) {
                case LC:
                    d->code[1] = '0'; // 8'0"
                    return;
                case DC:
                    d->code[1] = '2'; // 8'6"
                    return;
                case HC:
                    d->code[1] = '5'; // 9'6"
                    return;
                case MC:
                    d->code[1] = '4'; // D 9'0" with width < 2.5m  - or -  4 9'0" with width 8'" ?
                    return;
                case HalfDC:
                    d->code[1] = '8'; // 4'3"
                    return;
                case HalfLC:
                    d->code[1] = '9'; // <= 4'0"
                    return;
                case UnknownHeight:
                    d->code[1] = 'X';
                    return;
            }
            Q_ASSERT(false);
            return;
        }
    }
    Q_ASSERT(false);
    return;
}

// static
ange::units::Length IsoCode::physicalHeight(IsoCode::IsoHeight height) {
    switch (height) {
        case IsoCode::DC:
            return 8.5 * feet;  // 8'6"
        case IsoCode::HC:
            return 9.5 * feet;  // 9'6"
        case IsoCode::HalfLC:
            return 4.0 * feet;  // <= 4'0"
        case IsoCode::HalfDC:
            return 4.25 * feet;  // 4'3"
        case IsoCode::LC:
            return 8.0 * feet;  // 8'0"
        case IsoCode::MC:
            return 9.0 * feet;  // 9'0"
        case IsoCode::UnknownHeight:
            return 9.5 * feet;  // 9'6" - worst possible assumed
    }
    Q_ASSERT(false);
    return 9.5 * feet;  // 9'6" - worst possible assumed
}

ange::units::Length IsoCode::physicalHeight() const {
    return physicalHeight(height());
}

bool IsoCode::reefer() const {
    switch (standard()) {
        case InvalidCode:
            return false;
        case Standard1984: {
            switch (d->code.at(2).unicode()) {
                // 30  3  Thermal containers, Refrigerated, expendable refrigerant  R3
                // 31  3  Thermal containers, Refrigerated, Mechanically refrigerated   R0
                // 32  3  Thermal containers, Refrigerated, Refrigerated and heated  R1
                // 40  4  Thermal containers, Refrigerated and/or heated with removable equipment appliance located externally  H0
                // 41  4  Thermal containers, Refrigerated and/or heated with removable equipment appliance located externally  H0
                // 42  4  Thermal containers, Refrigerated and/or heated with removable equipment appliance located externally  H0
                case '3':
                case '4': {
                    switch (d->code.at(3).unicode()) {
                        case '0':
                        case '1':
                        case '2':
                            return true;
                        default:
                            return false;
                    }
                }
                default:
                    return false;
            }
        }
        case Standard1995: {
            switch (d->code.at(2).unicode()) {
                // H0  Refrigerated or heated with removable equipment located externally; heat transfer coefficient K=0.4W/M2.K  HR
                // H1  Refrigerated or heated with removable equipment located internally  HR
                // H2  Refrigerated or heated with removable equipment located externally; heat transfer coefficient K=0.7W/M2.K  HR
                // H5  Insulated - Heat transfer coefficient K=0.4W/M2.K  HI  - no reefer!
                // H6  Insulated - Heat transfer coefficient K=0.7W/M2.K  HI  - no reefer!
                case 'H': {
                    switch (d->code.at(3).unicode()) {
                        case 'R':
                        case '0':
                        case '1':
                        case '2':
                            return true;
                        default:
                            return false;
                    }
                }
                // R0  Integral Reefer - Mechanically refrigerated  RE
                // R1  Integral Reefer - Mechanically refrigerated and heated  RT
                // R2  Integral Reefer - Self-powered mechanically refrigerated  RS
                // R3  Integral Reefer - Self-powered mechanically refrigerated and heated  RS
                case 'R': {
                    switch (d->code.at(3).unicode()) {
                        case 'E':
                        case 'T':
                        case 'S':
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                            return true;
                        default:
                            return false;
                    }
                }
                default:
                    return false;
            }
        }
    }
    Q_ASSERT(false);
    return false;
}

void IsoCode::setReefer(bool reefer) {
    if (reefer == this->reefer()) {
        return;
    }
    Standard standard = this->standard();
    if (standard == InvalidCode) {
        d->makeCodeStandard();
        standard = this->standard();
    }
    switch (standard) {
        case InvalidCode:
            Q_ASSERT(false);
            return;
        case Standard1984: {
            if (reefer) {  // xx32 - Reefer/heated
                d->code[2] = '3';
                d->code[3] = '2';
            } else {  // xx00 - Dry Van
                d->code[2] = '0';
                d->code[3] = '0';
            }
            return;
        }
        case Standard1995: {
            if (reefer) {  // xxRT - Mechanically refrigerated and heated  RT
                d->code[2] = 'R';
                d->code[3] = 'T';
            } else {  // xxGP
                d->code[2] = 'G';
                d->code[3] = 'P';
            }
            return;
        }
    }
    Q_ASSERT(false);
    return;
}

bool IsoCode::isBundlable() const {
    if (isCollapsible()) {
        return true;
    }
    switch (standard()) {
        case InvalidCode:
            return false;
        case Standard1984: {
            switch (d->code.at(2).unicode()) {
                case '6': { // platform
                    switch (d->code.at(3).unicode()) {
                        case '0': // platform
                            return true;
                        default:
                            return false;
                    }
                }
                default:
                    return false;
            }
        }
        case Standard1995: {
            switch (d->code.at(2).unicode()) {
                case 'P': { // platform
                    switch (d->code.at(3).unicode()) {
                        case '0': // platform, no ends/posts
                        case 'L': // platform, no ends/posts
                            return true;
                        default:
                            return false;
                    }
                }
                default:
                    return false;
            }
        }
    }
    Q_ASSERT(false);
    return false;
}

bool IsoCode::isCollapsible() const {
    switch (standard()) {
        case InvalidCode:
            return false;
        case Standard1984: {
            switch (d->code.at(2).unicode()) {
                case '6': { // platform
                    switch (d->code.at(3).unicode()) {
                        case '3': // With complete and folding ends
                        case '4': // With folding free-standing posts
                            return true;
                        default:
                            return false;
                    }
                }
                default:
                    return false;
            }
        }
        case Standard1995: {
            switch (d->code.at(2).unicode()) {
                case 'P': { // platform
                    switch (d->code.at(3).unicode()) {
                        case 'C': // folding (collapsible)
                        case '3': // folding complete end structure
                        case '4': // folding posts
                        case '8': // ???
                        case '9': // ???
                            return true;
                        default:
                            return false;
                    }
                }
                default:
                    return false;
            }
        }
    }
    Q_ASSERT(false);
    return false;
}

bool IsoCode::isOpen() const {
    switch (standard()) {
        case InvalidCode:
            return false;
        case Standard1984: {
            switch (d->code.at(2).unicode()) {
                case '1': // vented
                case '5': // open top
                case '6': // platform
                    return true;
                default:
                    return false;
            }
        }
        case Standard1995: {
            switch (d->code.at(2).unicode()) {
                case 'P': // platform
                case 'U': // open top
                case 'V': // vented
                    return true;
                default:
                    return false;
            }
        }
    }
    Q_ASSERT(false);
    return false;
}

uint qHash(const IsoCode& isoCode) {
    return qHash(isoCode.code());
}

QList<IsoCode::IsoLength> IsoCode::knownLengths() {
    QList<IsoCode::IsoLength> list;
    list << Twenty;
    list << Fourty;
    list << FourtyFive;
    list << FiftyThree;
    return list;
}

QList< IsoCode::IsoLength > IsoCode::allLengths() {
    QList<IsoCode::IsoLength> list(knownLengths());
    list << UnknownLength;
    return list;
}

QString IsoCode::lengthToString(IsoCode::IsoLength length) {
    switch (length) {
        case UnknownLength:
            return "Unknown";
        case Twenty:
            return "20";
        case Fourty:
            return "40";
        case FourtyFive:
            return "45";
        case FiftyThree:
            return "53";
    }
    Q_ASSERT(false);
    return QString::number(length);
}

QList<IsoCode::IsoHeight> IsoCode::knownHeights() {
    QList<IsoCode::IsoHeight> list;
    list << DC;
    list << HC;
    return list;
}

QString IsoCode::heightToString(IsoCode::IsoHeight height) {
    switch (height) {
        case UnknownHeight:
            return "Unknown";
        case DC:
            return "DC";
        case HC:
            return "HC";
        case HalfLC:
            return "½LC";
        case HalfDC:
            return "½DC";
        case LC:
            return "LC";
        case MC:
            return "MC";
    }
    Q_ASSERT(false);
    return QString::number(height);
}

}} // namespace ange::containers
