#ifndef ANGE_CONTAINERS_DANGEROUS_GOODS_CODE_H
#define ANGE_CONTAINERS_DANGEROUS_GOODS_CODE_H

#include "angecontainers_export.h"

#include <QDebug>
#include <QSharedDataPointer>
#include <QMetaType>

class QString;

namespace ange {
namespace containers {

class ANGECONTAINERS_EXPORT DangerousGoodsCode {

public:

    /**
     * @param imdg_class string representing the imdg class
     * @param unnumber string representing the unnumber The syntax is "\d{4}L?", e.g. 1906 or 1001L
     * where "L" indicates a limited quantity
     */
    DangerousGoodsCode(const QString& unnumber);

    DangerousGoodsCode(const DangerousGoodsCode& copy);

    /**
     * Default constructor. constructs a dummy dg code
     */
    DangerousGoodsCode();

    ~DangerousGoodsCode();

    /**
     * @returns the unnumber for this code
     */
    QString unNumber() const;

    /**
     * @returns the main imdg class for this code
     */
    QString imdgClass() const;

    /**
     * @returns the imdg classes for this code
     */
    QStringList imdgClasses() const;

    /**
     * @returns whether this dangerous good is limited in quantity (meaning that it can
     * be treated as a class 9
     */
    bool isLimitedQuantity() const;

    const DangerousGoodsCode& operator=(const DangerousGoodsCode& rhs);

    bool operator==(const DangerousGoodsCode& rhs) const;
    bool operator!=(const DangerousGoodsCode& rhs) const;

private:
    class private_t;
    QSharedDataPointer<private_t> d;
};

ANGECONTAINERS_EXPORT QDebug operator<<(QDebug dbg, const DangerousGoodsCode& dg);

} //namespace containers
} //namespace ange

Q_DECLARE_METATYPE(ange::containers::DangerousGoodsCode)
Q_DECLARE_METATYPE(QList<ange::containers::DangerousGoodsCode>)

#endif // DANGEROUS_GOODS_CODE_H
