#ifndef LIBANGECONTAINER_CONTAINER_H
#define LIBANGECONTAINER_CONTAINER_H

#include "angecontainers_export.h"
#include "dangerousgoodscode.h"
#include "equipmentnumber.h"
#include "handlingcode.h"
#include "isocode.h"

#include <ange/units/units.h>
#include <ange/utils/identifiable.h>

#include <QDebug>
#include <QObject>
#include <QMetaType>

namespace ange {
namespace containers {

class Oog;
class ContainerPrivate;
class DangerousGoodsCode;
class ContainerUndoBlob;
class ContainerBundle;
class ContainerBundlePrivate;
class ContainerBundleUndoBlob;

/**
 * Describes a container during a voyage
 */
class ANGECONTAINERS_EXPORT Container : public QObject, public ange::utils::Identifiable<Container> {

    Q_OBJECT

public:
    /**
     * @param equipmentNumber external id of container
     * @param weight weight in kg
     * @param isoCode
     */
    Container(const containers::EquipmentNumber& equipmentNumber, units::Mass weight, const containers::IsoCode& isoCode);

    /**
     * Functions allowing the class to provide its data in a form suitable
     * for being kept in the undo stack.
     */
    static Container* createFromUndoData(ContainerUndoBlob* undoData, int id = -1);

    ~Container(); // Needed by QScopedPointer

    /**
     * @returns the data of the Container in a form suitable to be kept
     * in the undo stack.
     * It is subsequently the caller's responsibility that the data be
     * deleted (using delete_undo_data(ContainerUndoData*)
     */
    virtual ContainerUndoBlob* undoData() const;

    /**
     * deletes the undo data. needed because we need to be
     * able to access the destructor from outside.
     * the reason why the destructor cannot be public is that
     * it would expose the size of the class causing ABI breaks
     * at each modification of Oog
     */
    static void deleteUndoData(ContainerUndoBlob* undoData);

    /**
     * Live status (generally for reefers, but also other types such as heated tanks).
     * Auto converts sensibly to a boolean: NonLive is false, Live is true.
     */
    enum LiveStatus {
        NonLive = 0,
        Live = 1
    };

    /**
     * Enum listing the features characterizing a container.
     * Is not the same as the list of columns on the container list, this is only used for the featureChanged() signal.
     */
    enum Feature {
        EquipmentNumber,
        Weight,
        VGM,
        IsoCode,
        Reefer, // both reefer and live, derived from iso code
        Height, // partly derived from iso code
        Length, // derived from iso code
        Oog,
        LiveFeature, // live or not. Note that non-reefers can also be live, e.g. heated tanks
        Temperature,
        Empty,
        DangerousGoodsCodes,
        Carrier,
        LoadPort,
        DischargePort,
        PlaceOfDelivery,
        BookingNumber,
        HandlingCodes,
    };

    containers::EquipmentNumber equipmentNumber() const ;

    /**
     * @return weight in kg
     */
    units::Mass weight() const;

    /**
     * changes the weight of the container
     */
    void setWeight(units::Mass weight);

    /**
     * @return VGM status
     */
    bool weightIsVerified() const;

    /**
     * sets the VGM status of the container
     */
    void setWeightIsVerified(bool);

    /**
     * @return the isocode for the container, which describes the containers capabilities
     */
    const containers::IsoCode& isoCode() const;

    /**
     * Set the ISO code for the container
     */
    void setIsoCode(const containers::IsoCode& isoCode);

    /**
     * @return whether this container is "open" (for DG rules)
     * Result is taken from the ISO code
     */
    bool open() const;

    /**
     * @return true if container is a collapsible and empty flatrack
     * Result is taken from empty and the ISO code
     */
    bool isCollapsible() const;

    /**
     * @return true if container is a plain and empty platforn, or an collapsible and empty flatrack
     * Result is taken from empty and the ISO code
     */
    bool isBundlable() const;

    /**
     * @return length, see IsoCode::length()
     */
    IsoCode::IsoLength isoLength() const;

    /**
     * sets the iso height
     */
    void setIsoLength(IsoCode::IsoLength length);

    /**
     * @return length in meters
     */
    units::Length physicalLength() const;

    /**
     * sets the iso height
     */
    void setIsoHeight(IsoCode::IsoHeight height);

    /**
     * @return height in meters
     */
    units::Length physicalHeight() const;

    /**
     * Sets the height in meters. In general, this should only be used
     * if the height of the container in meters somehow differs from the
     * height that can be deducted from the iso code and OOG data.
     *
     * Use the special value '-1' to denote that the height matching
     * iso code can be used.
     * @param height new height in meters.
     */
    void setPhysicalHeight(units::Length height);

    /**
     * @returns oog info
     */
    containers::Oog oog() const;

    /**
     * Sets the OOG.
     */
    void setOog(const containers::Oog& oog);

    /**
     * @return whether or not the container is live
     * Return value can be used as boolean where true means that the container is live.
     */
    LiveStatus live() const;

    /**
     * changes the live status of the container
     */
    void setLive(LiveStatus isLive);

    /**
     * Temperature of temperature controlled containers like reefers or heated tanks.
     * If live() is Live but the temperature is unknown this will return unknownTemperature().
     * @return temperature in degrees Celsius, only relevant if live() is Live
     */
    double temperature() const;

    /**
     * Sets the temperature to @param temperature degrees Celcius
     * Set to unknownTemperature() if the temperature is unknown.
     */
    void setTemperature(double temperature);

    /**
     * The magic value that signals that the temperature is unknown, this is used for containers known to be live but
     * where the temperature is unknown.
     */
    static double unknownTemperature() {  // Implemented with function as C++ (pre C++11) does not allow doubles as constants
        return 999.0;
    }

    /**
     * @true if the container is empty
     */
    bool empty() const;

    /**
     * changes the empty status of the container
     */
    void setEmpty(bool empty);

    /**
     * @return true if container contains dangerous goods
     */
    bool isDangerous() const;

    /**
     * @return the list of dangerous_goods_codes
     */
    const QList<DangerousGoodsCode>& dangerousGoodsCodes() const;

    /**
     * sets the equipment number
     */
    void setEquipmentNumber(const containers::EquipmentNumber& equipmentNumber);

    /**
     * sets the booking number
     */
    void setBookingNumber(const QString& bookingNumber);

    /**
     * changes the load port (string) encoded in the container. This should be called
     * if the information is somehow wrong.
     */
    void setLoadPort(const QString& unCode);

    /**
     * sets the carrier code
     */
    void setCarrierCode(const QString& carrierCode);

    /**
     * changes the discharge port encoded in the container. THis should be used if the
     * container for one or another reason need to have a different 'endpoint'.
     * Like should be taken to hongkong instead of barcelona.
     */
    void setDischargePort(const QString& uncode);

    /**
     * @returns the uncode for the load port for this container.
     */
    QString loadPort() const;

    /**
     * @returns the discharge port for this container.
     */
    QString dischargePort() const;

    /**
     * @returns the booking number
     */
    QString bookingNumber() const;

    /**
     * Add a dangerous goods code
     */
    void addDangerousGoodsCode(const DangerousGoodsCode& dangerousGoodsCode);

    /**
     * Replace dangerous goods code list
     */
    void setDangerousGoodsCodes(const QList<DangerousGoodsCode>& dangerousGoodsCodes);

    /**
     * These codes are called "Stow Codes" in the UI.
     *
     * @return handling codes for this container
     */
    QList<HandlingCode> handlingCodes() const;

    /**
     * @param handlingCodes new value for handlingcodes. The old ones are cleared
     */
    void setHandlingCodes(QList<HandlingCode> handlingCodes);

    /**
     * @param handlingCode new value to append to handling codes. The old ones are kept
     */
    void addHandlingCode(HandlingCode handlingCode);

    /**
     * @returns the carrier's code
     */
    QString carrierCode() const;

    /**
     * @returns the bundle parent or 0 if not part of a bundle
     */
    Container* bundleParent() const;

    /**
     * @return the containers bundled upon this container or a empty list if not.
     */
    QList<Container*> bundleChildren() const;

    /**
     * removes a (child) container to bundle
     * @param container to unbundle
     * @return if the container gets unbundled
     */
    bool unbundleContainer(Container* container);

    /**
     * adds a (child) container to bundle
     * @param container to bundle
     * @return if the container succesfully gets bundled
     *
     * Note that false is also returned if the container is already bundled
     * by something else.
     */
    bool bundleContainer(Container* container);

    /**
     * @return the width of a standard containers (i.e. 8ft)
     */
    static units::Length standardContainerWidth();

    /**
     * @return the TEU equivalent of the container
     * @param HCequivalenceFactor is the equivalent in TEUs of a HC 40' container
     */
    double equivalentTEU(double HCequivalenceFactor = 2.0) const;

    /**
     * Set final place of delivery, also called final destination, which might be different
     * from discharge port in case of transhipments or hinterland transport.
     */
    void setPlaceOfDelivery(const QString& uncode);

    /**
     * returns final place of delivery, also called final destination, which might be different
     * from discharge port in case of transhipments or hinterland transport.
     */
    QString placeOfDelivery() const;

Q_SIGNALS:
    void featureChanged(ange::containers::Container* container, ange::containers::Container::Feature feature);
    void bundleStatusChanged(ange::containers::Container* container);

private:
    /**
     * Internal constructor for doing shared d-pointer magic.
     */
    Container(ContainerPrivate* dpoint, int id = -1);
    Container(ContainerUndoBlob* undoData, int id = -1);

private:
    QScopedPointer<ContainerPrivate> d;

};

ANGECONTAINERS_EXPORT QDebug operator<<(QDebug dbg, const Container& container);
ANGECONTAINERS_EXPORT QDebug operator<<(QDebug dbg, const Container* container);

}} // namespace ange::container

Q_DECLARE_METATYPE(ange::containers::Container::Feature)
Q_DECLARE_METATYPE(ange::containers::Container*)
Q_DECLARE_METATYPE(const ange::containers::Container*)

#endif // ANGE_CONTAINER_H
