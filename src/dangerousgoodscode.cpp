#include "dangerousgoodscode.h"
#include <QString>
#include "imoclassesperunnumber.h"
#include <QRegExp>

namespace ange {
namespace containers {

class DangerousGoodsCode::private_t : public QSharedData {
public:
    private_t(const QString& unnumber_description):
        m_imoClassesPerUnNumber(ImoClassesPerUnNumber::instance()) {
        QRegExp un_rx("(\\d{4})([lL])?");
        un_rx.indexIn(unnumber_description);
        if (un_rx.captureCount() >= 1) {
            m_unnumber = un_rx.cap(1);
            if (un_rx.captureCount() >= 2 && (un_rx.cap(2) == "l" || un_rx.cap(2) == "L")) {
                m_is_limited_quantity = true;
            } else {
                m_is_limited_quantity = false;
            }
        } else {
            m_is_limited_quantity = false;
            m_unnumber = "INVALID: " + unnumber_description;
        }
    }
    QString m_unnumber;
    bool m_is_limited_quantity;
    const QHash<QString, QStringList >* m_imoClassesPerUnNumber;
};

DangerousGoodsCode::DangerousGoodsCode(const QString& unnumber) :
    d(new DangerousGoodsCode::private_t(unnumber)) {
}

DangerousGoodsCode::DangerousGoodsCode() :
    d(new DangerousGoodsCode::private_t("")) {
}

DangerousGoodsCode::~DangerousGoodsCode() {
}

QString DangerousGoodsCode::imdgClass() const {
    if (!imdgClasses().isEmpty()) {
        return imdgClasses().first();
    } else {
        return QString();
    }
}

QStringList DangerousGoodsCode::imdgClasses() const {
    return d->m_imoClassesPerUnNumber->value(unNumber());
}

QString DangerousGoodsCode::unNumber() const {
    return d->m_unnumber;
}

QDebug operator<<(QDebug dbg, const ange::containers::DangerousGoodsCode& dg) {
    dbg.nospace();
    return dbg << "(" <<   dg.unNumber() << "," << dg.imdgClasses() << ")";
}

DangerousGoodsCode::DangerousGoodsCode(const ange::containers::DangerousGoodsCode& copy) : d(copy.d)
{}

const ange::containers::DangerousGoodsCode& DangerousGoodsCode::operator=(const ange::containers::DangerousGoodsCode& rhs) {
    if (d.constData() != rhs.d.constData()) {
        d = rhs.d;
    }
    return *this;
}

bool DangerousGoodsCode::operator==(const ange::containers::DangerousGoodsCode& rhs) const {
    return d->m_unnumber == rhs.d->m_unnumber && d->m_is_limited_quantity == rhs.d->m_is_limited_quantity;
}

bool DangerousGoodsCode::operator!=(const ange::containers::DangerousGoodsCode& rhs) const {
    return !(*this == rhs);
}

bool DangerousGoodsCode::isLimitedQuantity() const {
    return d->m_is_limited_quantity;
}

} //namespace containers
} //namespace ange


