#include <QtTest>

#include "isocode.h"

#include <QObject>

using ange::containers::IsoCode;

class IsoCodeTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testIsoCodes();
    void testIsoCodes_data();
};

void IsoCodeTest::testIsoCodes_data() {
    QTest::addColumn<QString>("isocodeString");
    QTest::addColumn<IsoCode::Standard>("standard");
    QTest::addColumn<IsoCode::IsoLength>("length");
    QTest::addColumn<IsoCode::IsoHeight>("height");
    QTest::addColumn<bool>("open");
    QTest::addColumn<bool>("reefer");
    QTest::addColumn<bool>("collapsible");
    QTest::addColumn<bool>("bundlable");

    // ISO 6346:1984
    // code_1984_34 code_1984_group  description  code_1995_34  comment
    // QTest::newRow("") << "" << IsoCode::Standard1984 << IsoCode::FOURTY << IsoCode::DC << false << false << false << false;
    // 00  0  General purpose, Opening(s) at one and/or both ends  G0
    QTest::newRow("4000") << "4000" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::LC << false << false << false << false;
    // 01  0  General purpose, Opening(s) at one or both ends plus “full” opening(s) on one or both sides    G2
    QTest::newRow("4200") << "4200" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::DC << false << false << false << false;
    // 02  0  General purpose, Opening(s) at one or both ends plus “partial” opening(s) on one or both sides    G3
    QTest::newRow("4102") << "4102" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::LC << false << false << false << false;
    // 03  0  General purpose, Opening(s) at one or both ends plus opening roof  G0
    QTest::newRow("4303") << "4303" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::DC << false << false << false << false;
    // 04  0  General purpose, Opening(s) at one or both ends plus opening roof, plus opening(s) at one or both sides    G2
    QTest::newRow("2204") << "2204" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::DC << false << false << false << false;
    // 10  1  Closed container, vented, Passive vents at upper part of cargo space – Total vent cross section area < 25 cm2/m of nominal container length  V0
    QTest::newRow("9510") << "9510" << IsoCode::Standard1984 << IsoCode::FourtyFive << IsoCode::HC << true << false << false << false;
    // 11  1  Closed container, vented, Passive vents at upper part of cargo space – Total vent cross-section area > 25 cm2/m of nominal container length  V0
    QTest::newRow("4411") << "4411" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::HC << true << false << false << false;
    //  20 x 8.0 13  1  Closed container, ventilated, Non-mechanical system, vents at lower and upper  parts of cargo space  V0
    QTest::newRow("4513") << "4513" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::HC << true << false << false << false;
    // 15  1  Closed container, ventilated, Mechanical ventilation system, located internally   V2
    QTest::newRow("4315") << "4315" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::DC << true << false << false << false;
    // 17  1  Closed container, ventilated, Mechanical ventilation system, located externally   V4
    QTest::newRow("2217") << "2217" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::DC << true << false << false << false;
    // 20  2  Thermal containers, Insulated  H5
    QTest::newRow("8320") << "8320" << IsoCode::Standard1984 << IsoCode::UnknownLength << IsoCode::DC << false << false << false << false;
    // 21  2  Thermal containers, Insulated  H5
    QTest::newRow("8421") << "8421" << IsoCode::Standard1984 << IsoCode::UnknownLength << IsoCode::HC << false << false << false << false;
    // 22  2  Thermal containers, Heated  H0
    QTest::newRow("2322") << "2322" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::DC << false << false << false << false;
    // 25  2  Named cargo containers, Livestock carrier  S0
    QTest::newRow("2325") << "2325" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::DC << false << false << false << false;
    // 26  2  Named cargo containers, Automobile carrier  S1
    QTest::newRow("2126") << "2126" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::LC << false << false << false << false;
    // 30  3  Thermal containers, Refrigerated, expendable refrigerant  R3
    QTest::newRow("4330") << "4330" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::DC << false << true << false << false;
    // 31  3  Thermal containers, Refrigerated, Mechanically refrigerated   R0
    QTest::newRow("4531") << "4531" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::HC << false << true << false << false;
    // 32  3  Thermal containers, Refrigerated, Refrigerated and heated  R1
    QTest::newRow("4432") << "4432" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::HC << false << true << false << false;
    // 40  4  Thermal containers, Refrigerated and/or heated with removable equipment appliance located externally  H0
    QTest::newRow("2340") << "2340" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::DC << false << true << false << false;
    // 41  4  Thermal containers, Refrigerated and/or heated with removable equipment appliance located externally  H0
    QTest::newRow("9441") << "9441" << IsoCode::Standard1984 << IsoCode::FourtyFive << IsoCode::HC << false << true << false << false;
    // 42  4  Thermal containers, Refrigerated and/or heated with removable equipment appliance located externally  H0
    QTest::newRow("9542") << "9542" << IsoCode::Standard1984 << IsoCode::FourtyFive << IsoCode::HC << false << true << false << false;
    // 50  5  Open-top container, Openings(s) at one or both ends  U0
    QTest::newRow("4450") << "4450" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::HC << true << false << false << false;
    // 51  5  Open-top container, Opening(s) at one or both ends, plus removable  top member(s) in end frame(s)  U1
    QTest::newRow("4551") << "4551" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::HC << true << false << false << false;
    // 52  5  Open-top container, Opening(s) at one or both ends, plus opening(s)  on one or both sides  U2
    QTest::newRow("3352") << "3352" << IsoCode::Standard1984 << IsoCode::UnknownLength << IsoCode::DC << true << false << false << false;
    // 53  5  Open-top container, Opening(s) at one or both ends, plus opening(s) on one or both sides plus removable top  member(s) in end frame(s)  U3
    QTest::newRow("8553") << "8553" << IsoCode::Standard1984 << IsoCode::UnknownLength << IsoCode::HC << true << false << false << false;
    // 60  6  Platform container    P0
    QTest::newRow("2960") << "2960" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::HalfLC << true << false << false << true;
    // 61  6  Platform container, With complete and fixed ends  P1
    QTest::newRow("2061") << "2061" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::LC << true << false << false << false;
    // 62  6  Platform container, With fixed free standing posts  P2
    QTest::newRow("2362") << "2362" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::DC << true << false << false << false;
    // 63  6  Platform container, With complete and folding ends  P3
    QTest::newRow("9563") << "9563" << IsoCode::Standard1984 << IsoCode::FourtyFive << IsoCode::HC << true << false << true << true;
    // 64  6  Platform container, With folding free-standing posts  P4
    QTest::newRow("4564") << "4564" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::HC << true << false << true << true;
    // 65  6  Platform container, With roof  P0
    QTest::newRow("2265") << "2265" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::DC << true << false << false << false;
    // 66  6  Platform container, With open top  P0
    QTest::newRow("6666") << "6666" << IsoCode::Standard1984 << IsoCode::UnknownLength << IsoCode::HalfDC << true << false << false << false;
    // 67  6  Platform container, With open top, open ends (skeletal)  P5
    QTest::newRow("7767") << "7767" << IsoCode::Standard1984 << IsoCode::UnknownLength << IsoCode::HalfDC << true << false << false << false;
    // 70  7  Tank container, For non-dangerous liquids, test pressure 0.45 bar  T0
    QTest::newRow("8770") << "8770" << IsoCode::Standard1984 << IsoCode::UnknownLength << IsoCode::HalfDC << false << false << false << false;
    // 71  7  Tank container, For non-dangerous liquids, test pressure 1.5 bar  T1
    QTest::newRow("2671") << "2671" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::HalfDC << false << false << false << false;
    // 72  7  Tank container, For non-dangerous liquids, test pressure 2.65 bar  T2
    QTest::newRow("2372") << "2372" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::DC << false << false << false << false;
    // 73  7  Tank container, For dangerous liquids, test pressure 1.5 bar  T3
    QTest::newRow("9573") << "9573" << IsoCode::Standard1984 << IsoCode::FourtyFive << IsoCode::HC << false << false << false << false;
    // 74  7  Tank container, For dangerous liquids, test pressure 2.65 bar  T4
    QTest::newRow("4274") << "4274" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::DC << false << false << false << false;
    // 75  7  Tank container, For dangerous liquids, test pressure 4.0 bar  T5
    QTest::newRow("2275") << "2275" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::DC << false << false << false << false;
    // 76  7  Tank container, For dangerous liquids, test pressure 6.0 bar  T6
    QTest::newRow("1676") << "1676" << IsoCode::Standard1984 << IsoCode::UnknownLength << IsoCode::HalfDC << false << false << false << false;
    // 77  7  Tank container, For dangerous gases, test pressure 10.5 bar  T7
    QTest::newRow("4177") << "4177" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::LC << false << false << false << false;
    // 78  7  Tank container, For dangerous gases, test pressure 22.0 bar  T8
    QTest::newRow("4078") << "4078" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::LC << false << false << false << false;
    // 79  7  Tank container, For dangerous gases, test pressure (to be developed)  T9
    QTest::newRow("9279") << "9279" << IsoCode::Standard1984 << IsoCode::FourtyFive << IsoCode::DC << false << false << false << false;
    // 80  8  Dry bulk containers  B0
    QTest::newRow("4380") << "4380" << IsoCode::Standard1984 << IsoCode::Fourty << IsoCode::DC << false << false << false << false;
    // 89  8  Dry bulk containers  B0
    QTest::newRow("2089") << "2089" << IsoCode::Standard1984 << IsoCode::Twenty << IsoCode::LC << false << false << false << false;
    // 90  9  Air/surface containers  A0
    QTest::newRow("8590") << "8590" << IsoCode::Standard1984 << IsoCode::UnknownLength << IsoCode::HC << false << false << false << false;


    // ISO 6346:1995
    // code_34  description  code_group
    // A0  Air-surface container  AS
    QTest::newRow("18A0") << "18A0" << IsoCode::Standard1995 << IsoCode::UnknownLength << IsoCode::HalfDC << false << false << false << false;
    // B0  Bulk - Closed  BU
    QTest::newRow("36B0") << "36B0" << IsoCode::Standard1995 << IsoCode::UnknownLength << IsoCode::UnknownHeight << false << false << false << false;
    // B1  Bulk - Airtight  BU
    QTest::newRow("28B1") << "28B1" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HalfDC << false << false << false << false;
    // B3  Bulk - Horizontal discharge, test pressure 1.50 bar  BK
    QTest::newRow("H0B3") << "H0B3" << IsoCode::Standard1995 << IsoCode::UnknownLength << IsoCode::LC << false << false << false << false;
    // B4  Bulk - Horizontal discharge, test pressure 2.65 bar  BK
    QTest::newRow("M8B4") << "M8B4" << IsoCode::Standard1995 << IsoCode::UnknownLength << IsoCode::HalfDC << false << false << false << false;
    // B5  Bulk - Tipping discharge, test pressure 1.50 bar  BK
    QTest::newRow("P5B5") << "P5B5" << IsoCode::Standard1995 << IsoCode::FiftyThree << IsoCode::HC << false << false << false << false;
    // B6  Bulk - Tipping discharge, test pressure 2.65 bar  BK
    QTest::newRow("C6B6") << "C6B6" << IsoCode::Standard1995 << IsoCode::UnknownLength << IsoCode::UnknownHeight << false << false << false << false;
    // G0  General - Openings at one or both ends  GP
    QTest::newRow("42G0") << "42G0" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << false << false << false << false;
    // G1  General - Passive vents at upper part of cargo space  GP
    QTest::newRow("22G1") << "22G1" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::DC << false << false << false << false;
    // G2  General - Openings at one or both ends + full openings on one or both sides  GP
    QTest::newRow("L4G2") << "L4G2" << IsoCode::Standard1995 << IsoCode::FourtyFive << IsoCode::MC << false << false << false << false;
    // G3  General - Openings at one or both ends + partial openings on one or both sides  GP
    QTest::newRow("P5G3") << "P5G3" << IsoCode::Standard1995 << IsoCode::FiftyThree << IsoCode::HC << false << false << false << false;
    // H0  Refrigerated or heated with removable equipment located externally; heat transfer coefficient K=0.4W/M2.K  HR
    QTest::newRow("42H0") << "42H0" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << false << true << false << false;
    // H1  Refrigerated or heated with removable equipment located internally  HR
    QTest::newRow("42H1") << "42H1" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << false << true << false << false;
    // H2  Refrigerated or heated with removable equipment located externally; heat transfer coefficient K=0.7W/M2.K  HR
    QTest::newRow("42H2") << "42H2" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << false << true << false << false;
    // H5  Insulated - Heat transfer coefficient K=0.4W/M2.K  HI
    QTest::newRow("42H5") << "42H5" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << false << false << false << false;
    // H6  Insulated - Heat transfer coefficient K=0.7W/M2.K  HI
    QTest::newRow("42H6") << "42H6" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << false << false << false << false;
    // P0  Flat or Bolter - Plain platform  PL
    QTest::newRow("29P0") << "29P0" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HalfLC << true << false << false << true;
    // P1  Flat or Bolter - Two complete and fixed ends  PF
    QTest::newRow("24P1") << "24P1" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::MC << true << false << false << false;
    // P2  Flat or Bolter - Fixed posts, either free-standing or with removable top member  PF
    QTest::newRow("PEP2") << "PEP2" << IsoCode::Standard1995 << IsoCode::FiftyThree << IsoCode::HC << true << false << false << false;
    // P3  Flat or Bolter - Folding complete end structure  PC
    QTest::newRow("42P3") << "42P3" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << true << false << true << true;
    // P4  Flat or Bolter - Folding posts, either free-standing or with removable top member  PC
    QTest::newRow("42P4") << "42P4" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << true << false << true << true;
    // P5  Flat or Bolter - Open top, open ends (skeletal)  PS
    QTest::newRow("42P5") << "42P5" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << true << false << false << false;
    // R0  Integral Reefer - Mechanically refrigerated  RE
    QTest::newRow("42R0") << "42R0" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << false << true << false << false;
    // R1  Integral Reefer - Mechanically refrigerated and heated  RT
    QTest::newRow("L5R1") << "L5R1" << IsoCode::Standard1995 << IsoCode::FourtyFive << IsoCode::HC << false << true << false << false;
    // R2  Integral Reefer - Self-powered mechanically refrigerated  RS
    QTest::newRow("42R2") << "42R2" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << false << true << false << false;
    // R3  Integral Reefer - Self-powered mechanically refrigerated and heated  RS
    QTest::newRow("42R3") << "42R3" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << false << true << false << false;
    // S0  Named cargo – Livestock carrier  SN
    QTest::newRow("CCS0") << "CCS0" << IsoCode::Standard1995 << IsoCode::UnknownLength << IsoCode::DC << false << false << false << false;
    // S1  Named cargo – Automobile carrier  SN
    QTest::newRow("NFS1") << "NFS1" << IsoCode::Standard1995 << IsoCode::UnknownLength << IsoCode::UnknownHeight << false << false << false << false;
    // S2  Named cargo – Live fish carrier  SN
    QTest::newRow("42S2") << "42S2" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << false << false << false << false;
    // T0  Tank - Non dangerous liquids, minimum pressure 45 kPa  TN
    QTest::newRow("28T0") << "28T0" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HalfDC << false << false << false << false;
    // T1  Tank - Non dangerous liquids, minimum pressure 150 kPa  TN
    QTest::newRow("28T1") << "28T1" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HalfDC << false << false << false << false;
    // T2  Tank - Non dangerous liquids, minimum pressure 265 kPa  TN
    QTest::newRow("28T2") << "28T2" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HalfDC << false << false << false << false;
    // T3  Tank - Dangerous liquids, minimum pressure 150 kPa  TD
    QTest::newRow("28T3") << "28T3" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HalfDC << false << false << false << false;
    // T4  Tank - Dangerous liquids, minimum pressure 265 kPa  TD
    QTest::newRow("28T4") << "28T4" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HalfDC << false << false << false << false;
    // T5  Tank - Dangerous liquids, minimum pressure 400 kPa  TD
    QTest::newRow("28T5") << "28T5" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HalfDC << false << false << false << false;
    // T6  Tank - Dangerous liquids, minimum pressure 600 kPa  TD
    QTest::newRow("28T6") << "28T6" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HalfDC << false << false << false << false;
    // T7  Tank - Gases, minimum pressure 910 kPa  TG
    QTest::newRow("25T7") << "25T7" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HC << false << false << false << false;
    // T8  Tank - Gases, minimum pressure 2200 kPa  TG
    QTest::newRow("25T8") << "25T8" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HC << false << false << false << false;
    // T9  Tank - Gases, minimum pressure to be decided  TG
    QTest::newRow("25T9") << "25T9" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::HC << false << false << false << false;
    // U0  Open Top - Openings at one or both ends  UT
    QTest::newRow("42U0") << "42U0" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << true << false << false << false;
    // U1  Open Top - Idem + removable top members in end frames  UT
    QTest::newRow("42U1") << "42U1" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << true << false << false << false;
    // U2  Open Top - Openings at one or both ends + openings at one or both sides  UT
    QTest::newRow("42U2") << "42U2" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << true << false << false << false;
    // U3  Open Top - Idem + removable top members in end frames  UT
    QTest::newRow("L5U3") << "L5U3" << IsoCode::Standard1995 << IsoCode::FourtyFive << IsoCode::HC << true << false << false << false;
    // U4  Open Top - Openings at one or both ends + partial on one and full at other side  UT
    QTest::newRow("L5U4") << "L5U4" << IsoCode::Standard1995 << IsoCode::FourtyFive << IsoCode::HC << true << false << false << false;
    // U5  Open Top - Complete, fixed side and end walls ( no doors )  UT
    QTest::newRow("L5U5") << "L5U5" << IsoCode::Standard1995 << IsoCode::FourtyFive << IsoCode::HC << true << false << false << false;
    // V0  General ventilated - Non-mechanical, vents at lower and upper parts of cargo space  VH
    QTest::newRow("42V0") << "42V0" << IsoCode::Standard1995 << IsoCode::Fourty << IsoCode::DC << true << false << false << false;
    // V2  General ventilated - Mechanical ventilation system located internally  VH
    QTest::newRow("20V2") << "20V2" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::LC << true << false << false << false;
    // V4  General ventilated - Mechanical ventilation system located externally  VH
    QTest::newRow("24V4") << "24V4" << IsoCode::Standard1995 << IsoCode::Twenty << IsoCode::MC << true << false << false << false;
}

void IsoCodeTest::testIsoCodes() {
    QFETCH(QString, isocodeString);
    QFETCH(IsoCode::Standard, standard);
    QFETCH(IsoCode::IsoLength, length);
    QFETCH(IsoCode::IsoHeight, height);
    QFETCH(bool, open);
    QFETCH(bool, reefer);
    QFETCH(bool, collapsible);
    QFETCH(bool, bundlable);

    IsoCode isocode(isocodeString);
    QCOMPARE(isocode.standard(), standard);
    QCOMPARE(isocode.length(), length);
    QCOMPARE(isocode.height(), height);
    QCOMPARE(isocode.isOpen(), open);
    QCOMPARE(isocode.reefer(), reefer);
    QCOMPARE(isocode.isCollapsible(), collapsible);
    QCOMPARE(isocode.isBundlable(), bundlable);
}

QTEST_GUILESS_MAIN(IsoCodeTest);

#include "isocodetest.moc"
