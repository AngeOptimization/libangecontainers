find_package(Qt5Test 5.2.0 REQUIRED CONFIG)

macro(make_test testbasename)
    add_executable(${testbasename} ${testbasename}.cpp)
    target_link_libraries(${testbasename}
        Qt5::Core
        Qt5::Test
        Containers
    )
    add_test(${testbasename} ${testbasename})
endmacro(make_test)

make_test(containerbundletest)
make_test(containertest)
make_test(isocodetest)
