#include <QtTest>

#include "container.h"

#include <QObject>

using namespace ange::units;

class ContainerBundleTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    void testBundleContainers();
};

using ange::containers::Container;
using ange::containers::IsoCode;
using ange::containers::EquipmentNumber;

void ContainerBundleTest::initTestCase() {
    qRegisterMetaType<ange::containers::Container*>("ange::containers::Container*");
}

void ContainerBundleTest::testBundleContainers() {
    // empty collapsible flatrack
    Container* container = new Container(EquipmentNumber("BUND1234567"), 2.5 * ton, IsoCode("42P3"));
    container->setEmpty(true);
    QVERIFY(container->empty());
    QVERIFY(container->isBundlable());
    QCOMPARE(container->bundleParent(), (Container*)0);
    QVERIFY(container->bundleChildren().empty());
    QCOMPARE(container->weight(), 2.5 * ton);
    ange::units::Length height_in_meters = container->physicalHeight();
    QCOMPARE(container->physicalHeight(), height_in_meters);

    // second empty collapsible flatrack bundled with first
    Container* container2 = new Container(EquipmentNumber("BUND2222222"), 2.5 * ton, IsoCode("42P3"));
    container2->setEmpty(true);
    QVERIFY(container2->isBundlable());
    QCOMPARE(container2->bundleParent(), (Container*)0);

    container->bundleContainer(container2);
    QCOMPARE(container2->bundleParent(), container);

    QVERIFY(container->empty());
    QVERIFY(container->isBundlable());
    QCOMPARE(container->bundleChildren().length(), 1);
    QCOMPARE(container->bundleChildren()[0], container2);
    QCOMPARE(container->bundleChildren()[0]->equipmentNumber(), EquipmentNumber("BUND2222222"));

    // bundle weight needs to be set manually
    QCOMPARE(container->weight(), 2.5 * ton);
    container-> setWeight(5.0 * ton);
    QCOMPARE(container->weight(), 5.0 * ton);

    // bundle height in meters needs to be set manually
    // a full slot height is first met after bundling 6 flatracks on top of the first
    QCOMPARE(container->physicalHeight(), height_in_meters);
    ange::units::Length IsoHeightwo_collapsed = height_in_meters * 2.0 / 7.0;
    container->setPhysicalHeight(IsoHeightwo_collapsed);
    QCOMPARE(container->physicalHeight(), IsoHeightwo_collapsed);

    // unbundle again
    QVERIFY(container->unbundleContainer(container2));
    QVERIFY(container->bundleChildren().empty());
    QCOMPARE(container2->bundleParent(), (Container*)0);

    delete container2;
    delete container;
}

QTEST_GUILESS_MAIN(ContainerBundleTest);

#include "containerbundletest.moc"
