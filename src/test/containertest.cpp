#include <QtTest>
#include "container.h"
#include "oog.h"

using namespace ange::containers;
using namespace ange::units;

class ContainerTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    void testUndoBlobs();
    void testChangeWeight();
    void testChangeWeightIsVerified();
    void testChangeHeight();
    void testChangeEquipmentNumber();
    void testChangeBookingNumber();
    void testChangeIsEmpty();
    void testChangeCarrierCode();
    void testChangeDischargePort();
    void testChangeHandlingCodes();
    void testOog();
};

using ange::containers::Container;
using ange::containers::IsoCode;
using ange::containers::EquipmentNumber;
using ange::containers::HandlingCode;
using ange::containers::ContainerUndoBlob;

void ContainerTest::initTestCase() {
    qRegisterMetaType<ange::containers::Container*>("ange::containers::Container*");
}

void ContainerTest::testUndoBlobs() {
    Container* container = new Container(EquipmentNumber("FAKZ 1234567"), 20 * ton, IsoCode("4200"));
    ContainerUndoBlob* undoblob = container->undoData();
    Container* container2 = Container::createFromUndoData(undoblob);
    Container::deleteUndoData(undoblob);
    QVERIFY(QString("FAKZ1234567") == container->equipmentNumber());
    QVERIFY(QString("FAKZ1234567") == container2->equipmentNumber());
    QCOMPARE(container->weight() / kilogram, 20000.0);
    QCOMPARE(container2->weight() / kilogram, 20000.0);
    QCOMPARE(container2->isoLength(), IsoCode::Fourty);
    QCOMPARE(container2->isoCode().height(), IsoCode::DC);
    QVERIFY(!container->oog());
    QVERIFY(!container2->oog());
    delete container;
    QVERIFY(QString("FAKZ1234567") == container2->equipmentNumber());
    QCOMPARE(container2->weight() / kilogram, 20000.0);
    QCOMPARE(container2->isoCode().height(), IsoCode::DC);
    delete container2;
}

void ContainerTest::testChangeWeight() {
    Container* container = new Container(EquipmentNumber("FAKE 123456"), 20 * ton, IsoCode("4200"));
    QSignalSpy spy(container, SIGNAL(featureChanged(ange::containers::Container*, ange::containers::Container::Feature)));
    QVERIFY(spy.isValid());
    container->setWeight(25 * ton);
    QCOMPARE(spy.count(), 1);
    QList<QVariant> arguments = spy.takeFirst();
    QCOMPARE(arguments.at(0).value<Container*>(), container);
    QCOMPARE(arguments.at(1).value<Container::Feature>(), Container::Weight);
    QCOMPARE(container->weight() / kilogram, 25 * ton / kilogram);
    QVERIFY(container->bookingNumber().isEmpty());
    delete container;
}

void ContainerTest::testChangeWeightIsVerified() {
    Container* container = new Container(EquipmentNumber("FAKE 123456"), 20 * ton, IsoCode("4200"));
    QSignalSpy spy(container, SIGNAL(featureChanged(ange::containers::Container*, ange::containers::Container::Feature)));
    QVERIFY(spy.isValid());
    container->setWeightIsVerified(true);
    QCOMPARE(spy.count(), 1);
    QList<QVariant> arguments = spy.takeFirst();
    QCOMPARE(arguments.at(0).value<Container*>(), container);
    QCOMPARE(arguments.at(1).value<Container::Feature>(), Container::VGM);
    QCOMPARE(container->weightIsVerified(), true);
    delete container;
}

void ContainerTest::testChangeHeight() {
    Container* container = new Container(EquipmentNumber("FAKE 123456"), 20 * ton, IsoCode("4200"));
    QSignalSpy spy(container, SIGNAL(featureChanged(ange::containers::Container*, ange::containers::Container::Feature)));
    QVERIFY(spy.isValid());
    container->setPhysicalHeight(2.5 * meter);
    QCOMPARE(spy.count(), 1);
    QList<QVariant> arguments = spy.takeFirst();
    QCOMPARE(arguments.at(0).value<Container*>(), container);
    QCOMPARE(arguments.at(1).value<Container::Feature>(), Container::Height);
    QCOMPARE(container->physicalHeight(), 2.5 * meter);
    QVERIFY(container->bookingNumber().isEmpty());
    delete container;
}

void ContainerTest::testChangeBookingNumber() {
    Container* container = new Container(EquipmentNumber("FAKE 123456"), 20 * ton, IsoCode("4200"));
    QSignalSpy spy(container, SIGNAL(featureChanged(ange::containers::Container*, ange::containers::Container::Feature)));
    QVERIFY(spy.isValid());
    QVERIFY(container->bookingNumber().isEmpty());
    container->setBookingNumber("SV1234");
    QCOMPARE(spy.count(), 1);
    QList<QVariant> arguments = spy.takeFirst();
    QCOMPARE(arguments.at(0).value<Container*>(), container);
    QCOMPARE(arguments.at(1).value<Container::Feature>(), Container::BookingNumber);
    QCOMPARE(container->bookingNumber(), QString("SV1234"));
    QVERIFY(container->dischargePort().isEmpty());
    delete container;
}

void ContainerTest::testChangeCarrierCode() {
    Container* container = new Container(EquipmentNumber("FAKE 123456"), 20 * ton, IsoCode("4200"));
    QSignalSpy spy(container, SIGNAL(featureChanged(ange::containers::Container*, ange::containers::Container::Feature)));
    QVERIFY(spy.isValid());
    QVERIFY(container->carrierCode().isEmpty());
    container->setCarrierCode("SUNE");
    QCOMPARE(spy.count(), 1);
    QList<QVariant> arguments = spy.takeFirst();
    QCOMPARE(arguments.at(0).value<Container*>(), container);
    QCOMPARE(arguments.at(1).value<Container::Feature>(), Container::Carrier);
    QCOMPARE(container->carrierCode(), QString("SUNE"));
    QVERIFY(container->dischargePort().isEmpty());
    delete container;
}

void ContainerTest::testChangeDischargePort() {
    Container* container = new Container(EquipmentNumber("FAKE 123456"), 20 * ton, IsoCode("4200"));
    QSignalSpy featurespy(container, SIGNAL(featureChanged(ange::containers::Container*, ange::containers::Container::Feature)));
    QVERIFY(featurespy.isValid());
    QVERIFY(container->dischargePort().isEmpty());
    container->setDischargePort("DKFRB");
    {
        QCOMPARE(featurespy.count(), 1);
        QList<QVariant> arguments = featurespy.takeFirst();
        QCOMPARE(arguments.size(), 2);
        QCOMPARE(arguments.at(0).value<Container*>(), container);
        QCOMPARE(arguments.at(1).value<Container::Feature>(), Container::DischargePort);
    }
    QCOMPARE(container->dischargePort(), QString("DKFRB"));
    QVERIFY(container->carrierCode().isEmpty());
}

void ContainerTest::testChangeEquipmentNumber() {
    Container* container = new Container(EquipmentNumber("FAKE 123456"), 20 * ton, IsoCode("4200"));
    QSignalSpy spy(container, SIGNAL(featureChanged(ange::containers::Container*, ange::containers::Container::Feature)));
    QVERIFY(spy.isValid());
    QVERIFY(container->equipmentNumber() == QString("FAKE 123456"));
    container->setEquipmentNumber(EquipmentNumber("SUNE1234567"));
    QCOMPARE(spy.count(), 1);
    QList<QVariant> arguments = spy.takeFirst();
    QCOMPARE(arguments.at(0).value<Container*>(), container);
    QCOMPARE(arguments.at(1).value<Container::Feature>(), Container::EquipmentNumber);
    QVERIFY(container->equipmentNumber() == QString("SUNE1234567"));
    QVERIFY(container->dischargePort().isEmpty());
    delete container;
}

void ContainerTest::testChangeIsEmpty() {
    Container* container = new Container(EquipmentNumber("FAKZ 1234567"), 20 * ton, IsoCode("4200"));
    QSignalSpy spy(container, SIGNAL(featureChanged(ange::containers::Container*, ange::containers::Container::Feature)));
    QVERIFY(spy.isValid());
    QVERIFY(!container->empty());
    container->setEmpty(true);
    QCOMPARE(spy.count(), 1);
    QList<QVariant> arguments = spy.takeFirst();
    QCOMPARE(arguments.at(0).value<Container*>(), container);
    QCOMPARE(arguments.at(1).value<Container::Feature>(), Container::Empty);
    QVERIFY(container->empty());
    QVERIFY(container->equipmentNumber() == QString("FAKZ1234567"));
    delete container;
}

void ContainerTest::testChangeHandlingCodes() {
    Container* container = new Container(EquipmentNumber("SSCU1234567"), 20 * ton, IsoCode("4200"));
    QSignalSpy spy(container, SIGNAL(featureChanged(ange::containers::Container*, ange::containers::Container::Feature)));
    QVERIFY(spy.isValid());
    QList<HandlingCode> handling_codes;
    handling_codes.append(HandlingCode("AFH"));
    handling_codes.append(HandlingCode("DRY"));
    handling_codes.append(HandlingCode("TOP"));
    container->setHandlingCodes(handling_codes);
    QCOMPARE(spy.count(), 1); // one signal emitted
    QList<QVariant> arguments = spy.takeFirst();
    QCOMPARE(arguments.at(0).value<Container*>(), container);
    QCOMPARE(container->handlingCodes().value(0), HandlingCode("AFH"));
    QCOMPARE(container->handlingCodes().value(1), HandlingCode("DRY"));
    QCOMPARE(container->handlingCodes().value(2), HandlingCode("TOP"));
    QCOMPARE(container->equipmentNumber(), EquipmentNumber("SSCU1234567"));
    delete container;
}

void ContainerTest::testOog() {
    Container container(EquipmentNumber("FAKE 123456"), 20 * ton, IsoCode("4200"));
    Oog oog;
    QCOMPARE(oog.top(), 0.0 * centimeter);
    oog.setTop(10 * centimeter);
    QCOMPARE(oog.top(), 10.0 * centimeter);
    QCOMPARE(container.oog().top(), 0.0 * centimeter);
    container.setOog(oog);
    QCOMPARE(container.oog().top(), 10.0 * centimeter);
    QVERIFY(container.oog());
}

QTEST_GUILESS_MAIN(ContainerTest);

#include "containertest.moc"
