#include "equipmentnumber.h"

#include <QRegExp>

namespace ange {
namespace containers {

EquipmentNumber::EquipmentNumber(const QString& equipment_number)
    : QString(equipment_number) {
    // Remove misplaced space in equipment number
    if (size() == 3 + 1 + 1 + 7 && at(4) == QChar(' ')) {  // Only do expensive RegExp() constructor and replace() when needed
        replace(QRegExp("^([A-Z]{3}[UJZ]) (\\d{7})$"), "\\1\\2");
    }
}

QList<EquipmentNumber> EquipmentNumber::generateRandomEquipmentNumbers(int length) {
    int wrap = 10000000;
    int wrapLength = 7;
    int startNo = (qrand() % wrap);
    QList <EquipmentNumber> eqNoList;
    for (int i = 0; i < length; ++i) {
        int eqNo = (startNo + i) % wrap;
        eqNoList.append(EquipmentNumber(QString("ZZZU%1").arg(eqNo, wrapLength, 10, QChar('0'))));
    }
    return eqNoList;
}

}} // namespace
