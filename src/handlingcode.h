#ifndef ANGE_CONTAINERS_HANDLING_CODE_H
#define ANGE_CONTAINERS_HANDLING_CODE_H

class QString;

#include "angecontainers_export.h"
#include <QDebug>
#include <QSharedDataPointer>
#include <QMetaType>

namespace ange {
namespace containers {

class ANGECONTAINERS_EXPORT HandlingCode {
public:

    /**
     * These codes are called "Stow Codes" in the UI.
     *
     * @param handling_code string representing the handling code from the list of COPRAR/BAPLIE codes
     * These are 1-2 digits, or 1-3 capital letters, i.e. 2, 50, T, FC, AFH, TOP
     * Also known as special stowage codes in the industry, but they cover more general handling aspects
     * TODO: There is no look-up of handling codes in a code list (yet)
     */
    explicit HandlingCode(const QString& handling_code);

    /**
     * Copy constructor
     */
    HandlingCode(const HandlingCode& copy);

    /**
     * Default constructor, empty content
     */
    HandlingCode();

    const HandlingCode& operator=(const HandlingCode& rhs);

    /**
     * @returns the handling code
     */
    QString code() const;

    // TODO  QString description() const; by look-up in code list

    ~HandlingCode();

    bool operator==(const HandlingCode& rhs) const;

    bool operator!=(const HandlingCode& rhs) const;

private:
    class Private;
    QSharedDataPointer<Private> d;
};

ANGECONTAINERS_EXPORT QDebug operator<<(QDebug dbg, const HandlingCode& hc);

} //namespace containers
} //namespace ange

Q_DECLARE_METATYPE(ange::containers::HandlingCode)
Q_DECLARE_METATYPE(QList<ange::containers::HandlingCode>)

#endif // HANDLING_CODE_H
