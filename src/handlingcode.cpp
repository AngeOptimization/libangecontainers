#include "handlingcode.h"
#include <QString>

namespace ange {
namespace containers {

class HandlingCode::Private : public QSharedData {
public:
    Private(const QString& handling_code): m_handling_code(handling_code)
    {}
    QString m_handling_code;
};

HandlingCode::HandlingCode(const QString& handling_code) :
    d(new HandlingCode::Private(handling_code)) {
}

HandlingCode::HandlingCode() :
    d(new HandlingCode::Private("")) {
}

HandlingCode::~HandlingCode() {
}

QString HandlingCode::code() const {
    return d->m_handling_code;
}

QDebug operator<<(QDebug dbg, const ange::containers::HandlingCode& hc) {
    dbg.nospace();
    return dbg << "(" << hc.code() << ")";
}

HandlingCode::HandlingCode(const ange::containers::HandlingCode& copy) : d(copy.d)
{}

const ange::containers::HandlingCode& HandlingCode::operator=(const ange::containers::HandlingCode& rhs) {
    if (d.constData() != rhs.d.constData()) {
        d = rhs.d;
    }
    return *this;
}

bool HandlingCode::operator==(const ange::containers::HandlingCode& rhs) const {
    return d->m_handling_code == rhs.d->m_handling_code;
}

bool HandlingCode::operator!=(const ange::containers::HandlingCode& rhs) const {
    return !(*this == rhs);
}

} //namespace containers
} //namespace ange
