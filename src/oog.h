#ifndef OOG_H
#define OOG_H

#include "angecontainers_export.h"

#include <ange/units/units.h>

#include <QSharedDataPointer>
#include <QVariant>

namespace ange {
namespace containers {

class ANGECONTAINERS_EXPORT Oog {

public:

    /**
     * Create Oog of all zeros
     */
    Oog();

    Oog(const Oog& other);

    ~Oog();

    Oog &operator=(const Oog &rhs);

    /**
     * Check if any of the values are non-zero (exact)
     */
    operator bool() const;

    /**
     * @return out of gauge to the front
     */
    ange::units::Length front() const;

    /**
     * @return out of gauge to the back
     */
    ange::units::Length back() const;

    /**
     * @return out of gauge to the right
     */
    ange::units::Length right() const;

    /**
     * @return out of gauge to the left
     */
    ange::units::Length left() const;

    /**
     * @return out of gauge to the top
     */
    ange::units::Length top() const;

    /**
     * @param front new value for 'front'
     */
    void setFront(ange::units::Length front);

    /**
     * @param back new value for 'back'
     */
    void setBack(ange::units::Length back);

    /**
     * @param right new value for 'right'
     */
    void setRight(ange::units::Length right);

    /**
     * @param left new value for 'left'
     */
    void setLeft(ange::units::Length left);

    /**
     * @param top new value for 'top'
     */
    void setTop(ange::units::Length top);

private:
    class OogData;
    QSharedDataPointer<OogData> d;

};

}} // namespace ange::container

Q_DECLARE_METATYPE(ange::containers::Oog)

#endif // OOG_H
